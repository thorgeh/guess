package main

import (
	"fmt"
	"log"
	"math/rand"
	"strings"

	"github.com/charmbracelet/bubbles/progress"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"gitlab.com/thorgeh/guess/numberinput"
	"gitlab.com/thorgeh/guess/theme"
)

var (
	titleText = `
█▀▀ █░█ █▀▀ █▀ █▀
█▄█ █▄█ ██▄ ▄█ ▄█`

	wonText = `
█▄█ █▀█ █░█   █░█░█ █▀█ █▄░█ █
░█░ █▄█ █▄█   ▀▄▀▄▀ █▄█ █░▀█ ▄`

	loseText = `
█▄█ █▀█ █░█   █░░ █▀█ █▀ █▀▀
░█░ █▄█ █▄█   █▄▄ █▄█ ▄█ ██▄`
)

func main() {
	if _, err := tea.NewProgram(initialModel(), tea.WithAltScreen()).Run(); err != nil {
		log.Fatal(err)
	}
}

type model struct {
	input         numberinput.Model
	progress      progress.Model
	randNum       int
	width, height int
	won           bool
	lost          bool
	tries         []int
}

func initialModel() model {
	in := numberinput.New()
	pb := progress.New(progress.WithGradient("#94e2d5", "#fab387"), progress.WithoutPercentage())

	return model{
		input:    in,
		progress: pb,
		randNum:  rand.Intn(100),
		// won:     true,
	}
}

func (m model) Init() tea.Cmd {
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {
	case tea.KeyMsg:
		if m.won || m.lost {
			return m, tea.Quit
		}
		switch msg.String() {
		case "ctrl+c", "q":
			return m, tea.Quit
		case "enter":

			v, err := m.input.Value()
			if err != nil {
				return m, nil
			}

			if v == m.randNum {
				m.won = true
				return m, nil
			}

			m.tries = append([]int{v}, m.tries...)
			m.input.Clear()

			if len(m.tries) == 10 {
				m.lost = true
				return m, nil
			}

			return m, nil
		}
	case tea.WindowSizeMsg:
		m.width, m.height = msg.Width, msg.Height

	}

	var input tea.Model
	input, cmd = m.input.Update(msg)
	if input, ok := input.(numberinput.Model); ok {
		m.input = input
	}

	return m, cmd
}

func (m model) View() string {
	if m.won {
		s := lipgloss.JoinVertical(
			lipgloss.Center,
			theme.WonStyle.Render(wonText),
			theme.HelpStyle.Render(fmt.Sprintf("Your number was: %d", m.randNum)),
			theme.HelpStyle.Render("Press any key to quit"),
		)

		return theme.BaseStyle.Render(lipgloss.Place(

			m.width,
			m.height,
			lipgloss.Center,
			lipgloss.Center,
			s,
		))
	}

	if m.lost {
		s := lipgloss.JoinVertical(
			lipgloss.Center,
			theme.ErrorStyle.Render(loseText),
			theme.HelpStyle.Render(fmt.Sprintf("The number was: %d, Loser", m.randNum)),
			theme.HelpStyle.Render("Press any key to quit"),
		)

		return theme.BaseStyle.Render(lipgloss.Place(

			m.width,
			m.height,
			lipgloss.Center,
			lipgloss.Center,
			s,
		))
	}

	title := theme.TitleStyle.Render(titleText)

	input := m.input.View()
	error := "\n"
	if _, err := m.input.Value(); err != nil {
		error = theme.ErrorStyle.Render("✖ Enter a number dipshit...\n")
	}
	input = lipgloss.PlaceHorizontal(
		lipgloss.Width(title),
		lipgloss.Left,
		input+"\n"+error,
	)

	status := "\n"
	if len(m.tries) != 0 {
		if m.tries[0] < m.randNum {
			status = theme.SmallerStyle.Render(fmt.Sprintf("%d is too small", m.tries[0]))
		} else {
			status = theme.BiggerStyle.Render(fmt.Sprintf("%d is too big", m.tries[0]))
		}
	}

	s := lipgloss.JoinVertical(lipgloss.Center, title, "\n\n", input, status)
	s = lipgloss.PlaceHorizontal(m.width, lipgloss.Center, s)

	tries := m.progress.ViewAs(float64(len(m.tries))/10) + theme.BaseStyle.Render(fmt.Sprintf(" %d/%d tries", len(m.tries), 10))
	help := theme.HelpStyle.Render("Press q to quit")

	space := strings.Repeat(" ", max(0, m.width-lipgloss.Width(tries)-lipgloss.Width(help)))
	bottom := help + space + tries

	space = strings.Repeat("\n", max(0, m.height-lipgloss.Height(s)-lipgloss.Height(bottom)))

	s = s + space + bottom

	return s
}

//func max(a, b int) int {
//	if a > b {
//		return a
//	}
//	return b
//}
