package numberinput

import (
	"strconv"
	"strings"

	"github.com/charmbracelet/bubbles/cursor"
	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/thorgeh/guess/theme"
)

type Model struct {
	input  textinput.Model
	number int
	err    error
}

func New() Model {
	in := textinput.New()
	in.Placeholder = "Your Number"
	in.Focus()
	in.Prompt = "→ "
	in.PromptStyle = theme.PromptStyle
	in.Cursor.Style = theme.CursorStyle
	in.CharLimit = 5
	in.PlaceholderStyle = theme.PlaceholderStyle
	in.TextStyle = theme.BaseStyle

	return Model{
		input: in,
	}
}

func (m Model) Init() tea.Cmd {
	return nil
}

func (m Model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	m.input, cmd = m.input.Update(msg)

	switch msg.(type) {
	case tea.KeyMsg:
		if len(m.input.Value()) == m.input.CharLimit {
			m.input.Cursor.SetMode(cursor.CursorHide)
		} else {
			m.input.Cursor.SetMode(cursor.CursorBlink)
		}

		if input := strings.TrimSpace(m.input.Value()); input != "" {
			m.number, m.err = strconv.Atoi(input)
		} else {
			m.err = nil
		}
	}

	return m, cmd
}

func (m Model) View() string {
	return m.input.View()
}

func (m Model) Value() (int, error) {
	return strconv.Atoi(strings.TrimSpace(m.input.Value()))
}

func (m *Model) Clear() {
	m.input.SetValue("")
}
