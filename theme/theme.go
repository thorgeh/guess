package theme

import "github.com/charmbracelet/lipgloss"

const (
	RoseWater = lipgloss.Color("#f5e0dc")
	Red       = lipgloss.Color("#f38ba8")
	Blue      = lipgloss.Color("#89b4fa")
	Crust     = lipgloss.Color("#11111b")
	Text      = lipgloss.Color("#cdd6f4")
	Base      = lipgloss.Color("#1e1e2e")
	Overlay1  = lipgloss.Color("#7f849c")
	Lavender  = lipgloss.Color("#b4befe")
	Green     = lipgloss.Color("#a6e3a1")
	Peach     = lipgloss.Color("#fab387")
	Teal      = lipgloss.Color("#94e2d5")
)

var (
	BaseStyle        = lipgloss.NewStyle().Foreground(Text)
	ErrorStyle       = BaseStyle.Copy().Foreground(Red)
	PromptStyle      = BaseStyle.Copy().Foreground(Blue)
	CursorStyle      = BaseStyle.Copy().Foreground(RoseWater).Background(Crust)
	PlaceholderStyle = BaseStyle.Copy().Foreground(Overlay1)
	TitleStyle       = BaseStyle.Copy().Foreground(Text).BorderForeground(Lavender).BorderStyle(lipgloss.RoundedBorder()).PaddingLeft(8).PaddingRight(8).PaddingTop(1).PaddingBottom(1)
	WonStyle         = BaseStyle.Copy().Foreground(Green)
	HelpStyle        = PlaceholderStyle.Copy()
	SmallerStyle     = BaseStyle.Copy().Foreground(Teal)
	BiggerStyle      = BaseStyle.Copy().Foreground(Peach)
)
